#pragma once

#include "lab2.h"
#include "lab2StringSurface.h"

namespace lab2_pleshkov {
    const int MAX_RUSSIAN_WORDS_LENGTH = 50;
    class RussianWordsSurface : public Surface<words_t > {
    private:
        distance_t levDist(distance_t i, distance_t j);
    public:
        distance_t distance(const words_t& a, const words_t& b);
        std::vector<words_t> lookup(const words_t& current);
        std::wstring currentA, currentB;
    };
}
