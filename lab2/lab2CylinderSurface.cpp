#include "lab2CylinderSurface.h"

using namespace lab2_pleshkov;

distance_t CylinderSurface::distance(const PlanarPoint& a, const PlanarPoint& b) {
    distance_t distanceXC;
    distance_t distanceX;
    distance_t distanceY;
    (a.x < b.x) ?
    (distanceX = b.x - a.x, distanceXC = surface[b.y].size() - b.x + a.x) :
    (distanceX = a.x - b.x, distanceXC = surface[a.y].size() - a.x + b.x);
    distanceY = (a.y < b.y) ? (b.y - a.y) : (a.y - b.y);
    return ((distanceX < distanceXC) ? distanceX : distanceXC) + distanceY;
}

std::vector<PlanarPoint> CylinderSurface::lookup(const PlanarPoint& current) {
    std::vector<PlanarPoint> result;
    distance_t x = current.x;
    distance_t y = current.y;
    if(x==0) {
        lookupAdder(PlanarPoint(surface[y].size()-1,y),result);
    }
    else {
        lookupAdder(PlanarPoint(x-1,y),result);
    }
    if((y>0)&&(x<surface[y-1].size())) {
        lookupAdder(PlanarPoint(x,y-1),result);
    }
    if(x==(surface[y].size()-1)) {
        lookupAdder(PlanarPoint(0,y),result);
    }
    else {
        lookupAdder(PlanarPoint(x+1,y),result);
    }
    if((y<(surface.size()-1))&&(x<surface[y+1].size())) {
        lookupAdder(PlanarPoint(x,y+1),result);
    }
    return result;
};
