#pragma once

#include "lab2.h"
#include "lab2SimpleSurface.h"
namespace lab2_pleshkov {
    class CylinderSurface : public Surface<PlanarPoint> {
    public:
        distance_t distance(const PlanarPoint& a, const PlanarPoint& b);
        std::vector<PlanarPoint> lookup(const PlanarPoint& current);
    };
}
