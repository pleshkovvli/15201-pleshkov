#include "lab1.hpp"
using namespace lab1_pleshkov;
//extern const myuint UINT_TRIT;

const Trit lab1_pleshkov::operator&(const Trit& lvalue, const Trit& rvalue)
{
	if((lvalue==False)||(rvalue==False))
	{
		return False;
	}
	if((lvalue==True)&&(rvalue==True))
	{
		return True;
	}
	return Unknown;
}

const Trit lab1_pleshkov::operator|(const Trit& lvalue, const Trit& rvalue)
{
	if((lvalue==True)||(rvalue==True))
	{
		return True;
	}
	if((lvalue==False)&&(rvalue==False))
	{
		return False;
	}
	return Unknown;
}

const Trit lab1_pleshkov::operator~(const Trit& value)
{
	if(value==True)
	{
		return False;
	}
	if(value==False)
	{
		return True;
	}
	return Unknown;
}
