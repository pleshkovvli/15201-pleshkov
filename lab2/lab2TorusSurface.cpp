#include "lab2TorusSurface.h"

using namespace lab2_pleshkov;

distance_t TorusSurface::distance(const PlanarPoint& a, const PlanarPoint& b) {
    distance_t distanceXC = 0;
    distance_t distanceYC = 0;
    distance_t distanceX = 0;
    distance_t distanceY = 0;
    distanceX = ((a.x < b.x) ? (b.x - a.x) : (a.x - b.x));
    distanceXC = ((a.x < b.x) ? (surface[b.y].size() - b.x + a.x) : (surface[a.y].size() - a.x + b.x));
    distanceY = ((a.y < b.y) ? (b.y - a.y) : (a.y - b.y));
    distanceYC = ((a.y < b.y) ? (surface.size() - b.y + a.y) : (surface.size() - a.y + b.y));
    distanceX = ((distanceX < distanceXC) ? distanceX : distanceXC);
    distanceY = ((distanceY < distanceYC) ? distanceY : distanceYC);
    return distanceX + distanceY;
}

std::vector<PlanarPoint> TorusSurface::lookup(const PlanarPoint& current) {
    std::vector<PlanarPoint> result;
    distance_t x = current.x;
    distance_t y = current.y;
    if(x==0) {
        lookupAdder(PlanarPoint(surface[y].size()-1,y),result);
    }
    else {
        lookupAdder(PlanarPoint(x-1,y),result);
    }
    if(y==0) {
        lookupAdder(PlanarPoint(x,surface.size()-1),result);
    }
    else {
        lookupAdder(PlanarPoint(x,y-1),result);
    }
    if(x==(surface[y].size()-1)) {
        lookupAdder(PlanarPoint(0,y),result);
    }
    else {
        lookupAdder(PlanarPoint(x+1,y),result);
    }
    if(y==(surface.size()-1)) {
        lookupAdder(PlanarPoint(x,0),result);
    }
    else {
        lookupAdder(PlanarPoint(x,y+1),result);
    }
    return result;
}
