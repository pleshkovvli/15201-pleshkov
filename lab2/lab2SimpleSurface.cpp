#include "lab2SimpleSurface.h"

using namespace lab2_pleshkov;

bool lab2_pleshkov::operator==(const PlanarPoint& a, const PlanarPoint& b) {
    return ((a.x == b.x) && (a.y == b.y));
}

bool lab2_pleshkov::operator!=(const PlanarPoint& a, const PlanarPoint& b) {
    return !operator==(a,b);
}

void Surface<PlanarPoint>::lookupAdder(const PlanarPoint& point, std::vector<PlanarPoint>& result) {
    if(surface[point.y][point.x]!=BLOCKED) {
        result.push_back(point);
    }
}

bool Surface<PlanarPoint>::isBlocked(const PlanarPoint& point) {
    return surface[point.y][point.x]==BLOCKED;
}

bool Surface<PlanarPoint>::checkPath(std::vector<PlanarPoint> path) {
    PlanarPoint last = path[1];
    for (PlanarPoint a: path) {
        if (distance(a, last) != 1) {
            return false;
        }
        if (isBlocked(a)) {
            return false;
        }
        last = a;
    }
    return true;
}
