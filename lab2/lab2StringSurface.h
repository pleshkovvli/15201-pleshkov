#pragma once

#include "lab2Serializer.h"
#include "lab2Surface.h"
namespace lab2_pleshkov {
    template <>
    class Surface<words_t > {
    protected:
        void lookupAdder(const words_t& point, std::vector<words_t>& result);
        bool isBlocked(const words_t& point);
    public:
        friend class Serializer<words_t>;
        virtual distance_t distance(const words_t& a, const words_t& b) = 0;
        bool checkPath(const std::vector<words_t >& path);
        virtual std::vector<words_t> lookup(const words_t& current) = 0;
    protected:
        std::vector<std::unordered_set<words_t> > surface;
    };
}
