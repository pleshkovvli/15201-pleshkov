#include "lab2StringSurface.h"

using namespace lab2_pleshkov;

void Surface<words_t >::lookupAdder(const words_t& point, std::vector<words_t>& result) {
    result.push_back(point);
}

bool Surface<words_t >::isBlocked(const words_t& point) {
    return (point=="!");
}

bool Surface<words_t >::checkPath(const std::vector<words_t >& path) {
    words_t last = path[1];
    for (const words_t& a: path) {
        if (distance(a, last) != 1) {
            return false;
        }
        if (isBlocked(a)) {
            return false;
        }
        last = a;
    }
    return true;
}
