#pragma once

#include "lab2.h"
#include "lab2Serializer.h"
#include "lab2SimpleSurface.h"

namespace lab2_pleshkov {
    template<>
    void Serializer<PlanarPoint>::readSurface(Surface<PlanarPoint> &sur, std::istream &is) {
        isFailed = false;
        char value;
        sur.surface.erase(sur.surface.begin(), sur.surface.end());
        sur.surface.push_back(std::vector<PlanarState>());
        while (is.get(value)) {
            if (value == '\n') {
                sur.surface.push_back(std::vector<PlanarState>());
                continue;
            } else if(value!=EMPTY && value!=BLOCKED) {
                throw BadInput("Wrong content");
            }
            sur.surface.back().push_back(static_cast<PlanarState>(value));
        }
    }

    template<>
    void Serializer<PlanarPoint>::writeSurface(Surface<PlanarPoint> &sur, std::ostream &os, std::vector<PlanarPoint>& path) {
        if(isFailed||!sur.checkPath(path)) {
            os << "Failed";
            return;
        }
        auto surface = sur.surface;
        for(PlanarPoint point : path) {
            surface[point.y][point.x] = PATH;
        }

        for (const auto &point : surface) {
            for (const auto &value: point) {
                char state = value;
                os << state;
            }
            os << std::endl;
        }
    }
}