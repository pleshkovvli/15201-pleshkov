#pragma once

#include "lab2.h"
#include "lab2Surface.h"
#include "lab2SimpleSurface.h"

namespace std
{
    template <>
    struct hash<lab2_pleshkov::PlanarPoint> {
        size_t operator()(lab2_pleshkov::PlanarPoint const & x) const noexcept {
            return (51 + std::hash<size_t >()(x.x)) * 51 + std::hash<size_t >()(x.y);
        }
    };
}
namespace lab2_pleshkov {
    template<typename T>
    class Robot {
    public:
        Robot(Surface <T> &sur);

        ~Robot();

        std::vector<T> search(const T &start, const T &finish, distance_t limit) throw(BadPath);

    protected:
        Surface <T> &surface;
    };
}
using namespace lab2_pleshkov;

template <typename T>
Robot<T>::Robot(Surface<T>& sur)
        :surface(sur){
}

template <typename T>
Robot<T>::~Robot() {

}

template <typename T>
std::vector<T> Robot<T>::search(const T& start, const T& finish, distance_t limit) throw(BadPath) {
    std::vector<T> path;
    std::unordered_set<T> badPoints;
    std::unordered_set<T> visitedPoints;
    T current = start;
    path.push_back(current);
    visitedPoints.insert(current);
    while(current!=finish) {
        distance_t min = UINT32_MAX;
        T a;
        //std::cout << "ok";
        for(T point : surface.lookup(current)) {
            distance_t distance = surface.distance(point, finish);
            if((distance < min)
               && (badPoints.find(point)==badPoints.end())
               && (visitedPoints.find(point)==visitedPoints.end())) {
                a = point;
                min = distance;
            }
        }
        if(min==UINT32_MAX) {
            for(T point : surface.lookup(current)) {
                distance_t distance = surface.distance(point, finish);
                if((distance < min)
                   && (badPoints.find(point)==badPoints.end())) {
                    a = point;
                    min = distance;
                }
            }
            if(min==UINT32_MAX) {
                throw BadPath();
            }
            if(a==path[path.size()-2]) {
                badPoints.insert(current);
                current = a;
                path.pop_back();
                continue;
            }
        }
        current = a;
        path.push_back(current);
        visitedPoints.insert(current);
        if(visitedPoints.size()>limit) {
            throw BadPath();
        }
    }
    return path;
}