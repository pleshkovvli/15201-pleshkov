#include "lab2final.h"

using namespace lab2_pleshkov;

int main(int argc, char* argv[]) {
    std::string inputName, outputName, limit, topology,start,finish;
    if(myparcer(argc,argv,inputName,outputName,limit,topology,start,finish)) {
        return 0;
    }
    try {
        if(topology=="words") {
            doFindPath<words_t >(inputName, outputName, limit, topology, start, finish);
        } else {
            doFindPath<PlanarPoint >(inputName, outputName, limit, topology, start, finish);
        }
    } catch (std::invalid_argument&) {
        std::cout << "Invalid arguments" << std::endl;
    } catch(BadInput& e) {
        std::cout << e.getMessage() << std::endl;
    }
}

