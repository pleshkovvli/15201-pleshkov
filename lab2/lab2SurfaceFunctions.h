#pragma once

#include "lab2Surface.h"
#include "lab2Robot.h"
#include "lab2Serializer.h"

namespace lab2_pleshkov {
    template <typename T>
    std::shared_ptr<Surface<T > > makeSurface(std::string surfaceType);

    template <typename T>
    void doFindPath(const std::string& inputName, const std::string& outputName,const std::string& limit,
                    const std::string& topology, const std::string& start, const std::string& finish) {
        std::fstream fileIn, fileOut;
        Serializer<T> serializer;
        std::shared_ptr<Surface<T> > surface = makeSurface<T >(topology);

        fileIn.open(inputName);
        if(!fileIn.good()) {
            throw BadInput("Input file does not exist");
        }
        serializer.readSurface(*surface,fileIn);
        fileIn.close();

        fileOut.open(outputName);
        if(!fileOut.good()) {
            throw BadInput("Output file does not exist");
        }
        Robot<T > robot(*surface);
        std::vector<T > path;
        try {
            path = robot.search(toPoint<T>(start), toPoint<T>(finish),std::stoi(limit));
        } catch (BadPath& ) {
            serializer.setFailed(true);
        }

        serializer.writeSurface(*surface,fileOut,path);
        fileOut.close();
    }
}