#include "lab2RussianWordsSurface.h"

using namespace lab2_pleshkov;

distance_t RussianWordsSurface::levDist(distance_t i, distance_t j) {
    if((i==0)&&(j==0)) {
        return 0;
    }
    if(i==0) {
        return j;
    }
    if(j==0) {
        return i;
    }
    distance_t a=levDist(i-1,j)+1;
    distance_t b=levDist(i,j-1)+1;
    distance_t c=levDist(i-1,j-1) + (currentA[i-1]!=currentB[j-1]);
    a = (a<b) ? a : b;
    a = (a<c) ? a : c;
    return a;
}

distance_t RussianWordsSurface::distance(const words_t& a, const words_t& b) {
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
    std::wstring w = cv.from_bytes(a);
    currentA = w;
    distance_t a_length = w.length();
    w = cv.from_bytes(b);
    currentB = w;
    distance_t b_length = w.length();
    return levDist(a_length,b_length);
}

std::vector<words_t> RussianWordsSurface::lookup(const words_t& current) {
    std::vector<words_t> result;
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
    std::wstring w = cv.from_bytes(current);
    for(words_t a: surface[w.length()]) {
        if(distance(current,a)==1) {
            lookupAdder(a,result);
        }
    }
    if(w.length()>0) {
        for (words_t a: surface[w.length() - 1]) {
            if (distance(current, a) == 1) {
                lookupAdder(a, result);
            }
        }
    }
    if(w.length()<(MAX_RUSSIAN_WORDS_LENGTH-1)) {
        for (words_t a: surface[w.length() + 1]) {
            if (distance(current, a) == 1) {
                lookupAdder(a, result);
            }
        }
    }
    return result;
};
