#pragma once

#include "lab2.h"
#include "lab2Serializer.h"
#include "lab2StringSurface.h"
#include "lab2RussianWordsSurface.h"

namespace lab2_pleshkov {
    template<>
    void Serializer<words_t >::readSurface(Surface<words_t > &sur, std::istream &is) {
        isFailed = false;
        std::string value;
        sur.surface.clear();
        std::unordered_set<words_t > empty;
        empty.insert("!");
        empty.clear();
        for(int i=0; i<MAX_RUSSIAN_WORDS_LENGTH; i++) {
            sur.surface.push_back(empty);
        }
        while (getline(is,value)) {
            std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
            std::wstring w = cv.from_bytes(value);
            sur.surface[w.length()].insert(value);
        }
    }

    template<>
    void Serializer<words_t >::writeSurface(Surface<words_t > &sur, std::ostream &os, std::vector<words_t >& path) {
        if(isFailed||!sur.checkPath(path)) {
            os << "Failed";
            return;
        }
        auto surface = sur.surface;
        std::unordered_set<words_t > buf;
        buf.insert("Path:");
        surface.push_back(buf);
        buf.clear();
        for(words_t point : path) {
            buf.insert(point);
            surface.push_back(buf);
            buf.clear();
        }
        bool flag = false;
        for(auto vec : surface) {
            if(vec.find("Path:")!=vec.end()) {
                flag = true;
            }
            if(flag) {
                os << *(vec.begin()) << std::endl;
            }
        }
    }
}