#pragma once

#include "lab2PlanarSurface.h"
#include "lab2CylinderSurface.h"
#include "lab2TorusSurface.h"
#include "lab2Robot.h"
#include "lab2SerializerSimple.h"
#include "lab2SerializerWords.h"
#include "lab2RussianWordsSurface.h"
#include "lab2SurfaceFunctions.h"
