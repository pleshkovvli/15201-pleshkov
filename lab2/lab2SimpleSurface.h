#pragma once

#include "lab2Surface.h"
#include "lab2Serializer.h"
namespace lab2_pleshkov {
    struct PlanarPoint {
        distance_t x;
        distance_t y;
        PlanarPoint(distance_t xv, distance_t yv) {
            x=xv;
            y=yv;
        }
        PlanarPoint() {
            x=0;
            y=0;
        }
    };

    bool operator==(const PlanarPoint& a, const PlanarPoint& b);
    bool operator!=(const PlanarPoint& a, const PlanarPoint& b);

    template <> class Surface<PlanarPoint> {
    protected:
        void lookupAdder(const PlanarPoint& point, std::vector<PlanarPoint>& result);
        bool isBlocked(const PlanarPoint& point);
    public:
        friend class Serializer<PlanarPoint>;
        virtual distance_t distance(const PlanarPoint& a, const PlanarPoint& b) = 0;
        bool checkPath(std::vector<PlanarPoint> path);
        virtual std::vector<PlanarPoint> lookup(const PlanarPoint& current) = 0;
    protected:
        std::vector<std::vector<PlanarState> > surface;
    };
}
