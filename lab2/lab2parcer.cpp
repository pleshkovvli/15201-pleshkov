#include "anyoption.h"
#include "lab2.h"



bool lab2_pleshkov::myparcer( int argc, char* argv[], std::string& inputName, std::string& outputName,
               std::string& limit, std::string& topology, std::string& start, std::string& finish )
{
    AnyOption *opt = new AnyOption();

    /* 2. SET PREFERENCES  */
    //opt->noPOSIX(); /* do not check for POSIX style character options */
    //opt->setVerbose(); /* print warnings about unknown options */
    opt->autoUsagePrint(true); /* print usage for bad options */

    /* 3. SET THE USAGE/HELP   */
    opt->addUsage( "" );
    opt->addUsage( "Usage: " );
    opt->addUsage( "" );
    opt->addUsage( " -h  --help         Prints this help " );
    opt->addUsage( " -s  --space        Name of file with source surface" );
    opt->addUsage( " -o  --out          Name of file with output surface " );
    opt->addUsage( " -l  --limit        Max length of path " );
    opt->addUsage( " -t  --topology     Type of surface  " );
    opt->addUsage( " --start            Start point: \"value\" for words, x,y - for simple surface " );
    opt->addUsage( " --finish           Finish point: \"value\" for words, x,y - for simple surface " );

    opt->setFlag(  "help", 'h' );   /* a flag (takes no argument), supporting long and short form */
    opt->setOption(  "space", 's' ); /* an option (takes an argument), supporting long and short form */
    opt->setOption(  "out", 'o' );      /* an option (takes an argument), supporting only long form */
    opt->setOption(  "limit", 'l' ); /* an option (takes an argument), supporting long and short form */
    opt->setOption(  "topology", 't' );      /* an option (takes an argument), supporting only long form */
    opt->setOption(  "start" ); /* an option (takes an argument), supporting long and short form */
    opt->setOption(  "finish" );      /* an option (takes an argument), supporting only long form */

    /* 5. PROCESS THE COMMANDLINE AND RESOURCE FILE */

    /* read options from a  option/resource file with ':' separated opttions or flags, one per line */
    opt->processFile( "/home/user/.options" );
    /* go through the command line and get the options  */
    opt->processCommandArgs( argc, argv );

    if( ! opt->hasOptions()) { /* print usage if no options */
        opt->printUsage();
        delete opt;
        return true;
    }

    //std::string inputName, outputName,limit,topology;
    /* 6. GET THE VALUES */
    if( opt->getFlag( "help" ) || opt->getFlag( 'h' ) ) {
        opt->printUsage();
    }

    if( opt->getValue( 's' ) != NULL) {
        inputName = opt->getValue('s');
    } else if( opt->getValue( "space" ) != NULL  ) {
        inputName = opt->getValue("space");
    } else {
        inputName = "space.txt";
    }

    if( opt->getValue( 'o' ) != NULL) {
        outputName = opt->getValue('o');
    } else if( opt->getValue( "out" ) != NULL  ) {
        outputName = opt->getValue("out");
    } else {
        outputName = "route.txt";
    }

    if( opt->getValue( 'l' ) != NULL) {
        limit = opt->getValue('l');
    } else if( opt->getValue( "limit" ) != NULL  ) {
        limit = opt->getValue("limit");
    } else {
        limit = "1000";
    }

    if( opt->getValue( 't' ) != NULL) {
        topology = opt->getValue('t');
    } else if( opt->getValue( "topology" ) != NULL  ) {
        topology = opt->getValue("topology");
    } else {
        topology = "planar";
    }

    if( opt->getValue( "start" ) != NULL) {
        start = opt->getValue( "start" );
    } else  {
        if(topology=="words") {
            start = "старт";
        } else {
            start = "1,1";
        }
    }

    if( opt->getValue( "finish" ) != NULL) {
        finish = opt->getValue("finish");
    } else  {
        if(topology=="words") {
            finish = "финиш";
        } else {
            finish = "2,2";
        }
    }

    delete opt;
    return false;
}
