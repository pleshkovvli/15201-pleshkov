#pragma once

#include <tuple>
#include <vector>
#include <iostream>
#include <limits>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <unordered_set>
#include <unordered_map>
#include <codecvt>
#include <locale>
#include <memory>
#include <fstream>


namespace lab2_pleshkov {
    using distance_t = unsigned long int;
    using words_t = std::string;
    enum PlanarState {EMPTY = '.', BLOCKED = '#', PATH = '*'};
    class BadPath : public std::exception {
    };

    class BadInput : public std::exception {
    public:
        BadInput() {
            message = "Unknown";
        }
        BadInput(std::string mes) {
            message = mes;
        }
        std::string& getMessage() {
            return message;
        }
    private:
        std::string message;
    };

    template <typename T>
    class Serializer;

    template <typename T>
    T toPoint(const std::string& str);

    bool myparcer( int argc, char* argv[], std::string& inputName, std::string& outputName,
                   std::string& limit, std::string& topology, std::string& start, std::string& finish );

}

