#pragma once

#include "lab2.h"
namespace lab2_pleshkov {
    template<typename T>
    class Surface {
    protected:
        virtual void lookupAdder(const T &point, std::vector<T> &result) = 0;
        virtual bool isBlocked(const T &point) = 0;
    public:
        virtual distance_t distance(const T &a, const T &b) = 0;
        virtual bool checkPath(const std::vector<T> &path) = 0;
        virtual std::vector<T> lookup(const T &current) = 0;
    };
}
