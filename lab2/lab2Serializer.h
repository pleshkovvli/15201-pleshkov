#pragma once

#include "lab2.h"
#include "lab2Surface.h"

namespace lab2_pleshkov {
    template <typename T>
    class Serializer {
    public:
        void readSurface(Surface<T>& surface, std::istream& is);
        void writeSurface(Surface<T>& sur, std::ostream& os, std::vector<T >& vector);
        void setFailed(bool value) {
            isFailed = value;
        }
    private:
        bool isFailed;
    };
}