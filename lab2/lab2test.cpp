#include "lab2final.h"
#include "googletest-master/googletest/include/gtest/gtest.h"

using namespace lab2_pleshkov;

void checkFiles(std::string testFile) {
    std::fstream testOut, actualOut;
    testOut.open(testFile);
    actualOut.open("out.txt");
    char testValue, actualValue;
    while(testOut.get(testValue)) {
        actualOut.get(actualValue);
        EXPECT_EQ(testValue,actualValue);
    }
}

TEST(testPlanar, testPlanarCase1Planar)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test1in -o out.txt -l 100 -t planar --start 1,1 --finish 2,3");
    checkFiles("test1out");
}

TEST(testPlanar, testPlanarCase1Torus)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test1in -o out.txt -l 100 -t torus --start 1,1 --finish 2,3");
    checkFiles("test1out");
}

TEST(testPlanar, testPlanarCase1Cylinder)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test1in -o out.txt -l 100 -t cylinder --start 1,1 --finish 2,3");
    checkFiles("test1out");
}

TEST(testPlanar, testPlanarCase2Planar)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test2in -o out.txt -l 100 -t planar --start 1,3 --finish 10,3");
    checkFiles("test2out");
}

TEST(testPlanar, testPlanarCase2Torus)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test2in -o out.txt -l 100 -t torus --start 1,3 --finish 10,3");
    checkFiles("test2out");

}


TEST(testPlanar, testPlanarCase2Cylinder)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test2in -o out.txt -l 100 -t cylinder --start 1,3 --finish 10,3");
    checkFiles("test2out");
}


TEST(testPlanar, testPlanarCase3Planar)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test3in -o out.txt -l 100 -t planar --start 10,5 --finish 5,3");
    checkFiles("test3out");
}

TEST(testPlanar, testPlanarCase3Torus)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test3in -o out.txt -l 100 -t torus --start 10,5 --finish 5,3");
    checkFiles("test3out");
}


TEST(testPlanar, testPlanarCase3Cylinder)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test3in -o out.txt -l 100 -t cylinder --start 10,5 --finish 5,3");
    checkFiles("test3out");
}

TEST(testPlanar, testPlanarCase4Planar)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test4in -o out.txt -l 100 -t planar --start 1,1 --finish 10,6");
    checkFiles("test4outPlanar");
}

TEST(testPlanar, testPlanarCase4Torus)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test4in -o out.txt -l 100 -t torus --start 1,1 --finish 10,6");
    checkFiles("test4outTorus");
}

TEST(testPlanar, testPlanarCase4Cylinder)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test4in -o out.txt -l 100 -t cylinder --start 1,1 --finish 10,6");
    checkFiles("test4outCylinder");
}

TEST(testWords, testWordsCase1)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test5in -o out.txt -l 100 -t words --start ааааа --finish ввввв");
    checkFiles("test5out");
}

TEST(testWords, testWordsCase2)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test6in -o out.txt -l 100 -t words --start а --finish жж");
    checkFiles("test6out");
}

TEST(testWords, testWordsCase3)
{
    std::cout << system("> out.txt");
    std::cout << system("./lab2exe -s test7in -o out.txt -l 100 -t words --start ааааааа --finish ууууеее");
    checkFiles("test7out");
}
