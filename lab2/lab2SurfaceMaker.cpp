#include "lab2SurfaceFunctions.h"
#include "lab2SimpleSurface.h"
#include "lab2TorusSurface.h"
#include "lab2CylinderSurface.h"
#include "lab2PlanarSurface.h"
#include "lab2RussianWordsSurface.h"
namespace lab2_pleshkov {

template <>
PlanarPoint toPoint<PlanarPoint >(const std::string& str) {
    PlanarPoint point;
    size_t next;
    point.x = std::stoi(str, &next);
    point.y = std::stoi(str.substr(next+1), nullptr);
    return point;
}

template <>
words_t toPoint<words_t >(const std::string& str) {
    return str;
}

template <>
std::shared_ptr<Surface<PlanarPoint> > makeSurface<PlanarPoint>(std::string surfaceType) {
    std::shared_ptr<Surface<PlanarPoint> > ptr;
    if(surfaceType=="torus") {
        ptr = std::shared_ptr<Surface<PlanarPoint> >(new TorusSurface);
    } else
    if(surfaceType=="cylinder") {
        ptr = std::shared_ptr<Surface<PlanarPoint> >(new CylinderSurface);
    } else if(surfaceType=="planar") {
        ptr = std::shared_ptr<Surface<PlanarPoint> >(new PlanarSurface);
    } else {
        throw BadInput("Wrong surface type");
    }
    return ptr;
}

template <>
std::shared_ptr<Surface<words_t > > makeSurface<words_t >(std::string surfaceType) {
    if(surfaceType!="words") {
        std::cout << "!";
    }
    return std::shared_ptr<Surface<words_t > >(new RussianWordsSurface);
}

}