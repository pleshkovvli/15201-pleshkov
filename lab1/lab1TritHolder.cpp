#include "lab1.hpp"
using namespace lab1_pleshkov;

TritSet::TritHolder::TritHolder(size_t index, TritSet& father)
:fatherSet(father)
{
	tritIndex=index;
	if(tritIndex>=fatherSet.tritCapacity)
	{
		tritValue=Unknown;
	}
	else
	{
		tritValue = fatherSet.getValue(index);
	}
}

TritSet::ConstTritHolder::ConstTritHolder(size_t index, const TritSet& father)
        :fatherSet(father)
{
    tritIndex=index;
    if(tritIndex>=fatherSet.tritCapacity)
    {
        tritValue=Unknown;
    }
    else
    {
        tritValue = fatherSet.getValue(index);
    }
}

TritSet::TritHolder::~TritHolder()
{

}

TritSet::ConstTritHolder::~ConstTritHolder()
{

}


TritSet::TritHolder& TritSet::TritHolder::operator=(const Trit& rvalue)
{
	if(tritIndex>=fatherSet.tritCapacity)
	{
		if(rvalue==Unknown)
		{
			return *this;
		}
		fatherSet.resize(tritIndex+1);
	}
	if(rvalue==tritValue)
	{
		return *this;
	}
	tritValue=rvalue;
    fatherSet.changeValue(tritIndex, rvalue);
	return *this;
}

TritSet::TritHolder& TritSet::TritHolder::operator=(const TritHolder& rvalue)
{
    if(*this==rvalue)
    {
        return *this;
    }
    if(tritIndex>=fatherSet.tritCapacity)
    {
        if(rvalue==Unknown)
        {
            return *this;
        }
        fatherSet.resize(tritIndex+1);

    }
    if(rvalue==tritValue)
    {
        return *this;
    }
    tritValue=rvalue;
    fatherSet.changeValue(tritIndex, rvalue);
    return *this;
}

TritSet::TritHolder& TritSet::TritHolder::operator=(const TritSet::ConstTritHolder& rvalue)
{
    if(*this==rvalue)
    {
        return *this;
    }
    if(tritIndex>=fatherSet.tritCapacity)
    {
        if(rvalue==Unknown)
        {
            return *this;
        }
        fatherSet.resize(tritIndex+1);
    }
    if(rvalue==tritValue)
    {
        return *this;
    }
    tritValue=rvalue;
    fatherSet.changeValue(tritIndex, rvalue);
    return *this;
}

TritSet::TritHolder::operator Trit() const
{
	return tritValue;
}

bool TritSet::TritHolder::operator==(const TritSet::TritHolder& other) const
{
    return (tritValue==other.tritValue);
}

bool TritSet::ConstTritHolder::operator==(const TritSet::ConstTritHolder& other) const
{
    return (tritValue==other.tritValue);
}

bool TritSet::TritHolder::operator!=(const TritSet::TritHolder& other) const
{
    return !operator==(other);
}

bool TritSet::ConstTritHolder::operator!=(const TritSet::ConstTritHolder& other) const
{
    return !operator==(other);
}

void TritSet::TritHolder::setPosition(size_t position)
{
    tritIndex=position;
    if(tritIndex>=fatherSet.tritCapacity)
    {
        tritValue=Unknown;
    }
    else
    {
        tritValue = fatherSet.getValue(position);
    }
}

void TritSet::ConstTritHolder::setPosition(size_t position)
{
    tritIndex=position;
}

TritSet::ConstTritHolder::operator Trit() const
{
    return tritValue;
}

TritSet& TritSet::TritHolder::getSet() const
{
	return fatherSet;
}

const TritSet& TritSet::ConstTritHolder::getSet() const
{
    return fatherSet;
}

size_t TritSet::ConstTritHolder::getIndex() const
{
    return tritIndex;
}

size_t TritSet::TritHolder::getIndex() const
{
    return tritIndex;
}

TritSet::ConstIterator::ConstIterator(const TritSet* set, size_t pos)
{
    if(set == nullptr)
    {
        throw std::invalid_argument("Set = nullptr");
    }
    mainSet = set;
	position=pos;
    holder = new ConstTritHolder(pos,*set);
}

TritSet::ConstIterator::ConstIterator(const ConstIterator& iterator)
        :mainSet(iterator.mainSet)
{
    position=iterator.position;
    holder = new ConstTritHolder(position,*mainSet);
}

TritSet::ConstIterator::~ConstIterator()
{
    delete holder;
}

bool TritSet::ConstIterator::operator==(const TritSet::ConstIterator& other) const
{
    bool equal = (mainSet == other.mainSet) && (position==other.position);
    return equal;
}

bool TritSet::ConstIterator::operator!=(const TritSet::ConstIterator& other) const
{
    return !operator==(other);
}

TritSet::ConstTritHolder& TritSet::ConstIterator::operator*() const
{
    holder->setPosition(position);
	return *holder;
}

TritSet::ConstTritHolder* TritSet::ConstIterator::operator->() const
{
    holder->setPosition(position);
    return holder;
}

const TritSet::ConstIterator& TritSet::ConstIterator::operator++()
{
	++position;
	return *this;
}

const TritSet::ConstIterator TritSet::ConstIterator::operator++(int)
{
    ConstIterator oldValue = *this;
    ++position;
    return oldValue;
}

TritSet::Iterator::Iterator(TritSet* set, size_t pos)
{
    if(set == nullptr)
    {
        throw std::invalid_argument("Set = nullptr");
    }
    mainSet = set;
    position = pos;
    holder = new TritHolder(pos, *set);
}

TritSet::Iterator::Iterator(const Iterator& iterator)
        :mainSet(iterator.mainSet)
{
    position = iterator.position;
    holder = new TritHolder(position,*mainSet);
}


TritSet::Iterator::~Iterator()
{
    delete holder;
}

TritSet::Iterator& TritSet::Iterator::operator=(const TritSet::Iterator& rvalue)
{
    if(*this == rvalue)
    {
        return *this;
    }
    mainSet=rvalue.mainSet;
    position=rvalue.position;
    delete holder;
    holder = new TritHolder(position,*mainSet);
    return *this;
}

bool TritSet::Iterator::operator==(const TritSet::Iterator& other) const
{
    bool equal = (mainSet == other.mainSet) && (position==other.position);
    return equal;
}

bool TritSet::Iterator::operator!=(const TritSet::Iterator& other) const
{
    return !operator==(other);
}

TritSet::TritHolder& TritSet::Iterator::operator*()
{
    holder->setPosition(position);
	return *holder;
}

TritSet::TritHolder* TritSet::Iterator::operator->()
{
    holder->setPosition(position);
    return holder;
}

const TritSet::Iterator& TritSet::Iterator::operator++()
{
	++position;
	return *this;
}

const TritSet::Iterator TritSet::Iterator::operator++(int)
{
    Iterator oldValue = *this;
    ++position;
    return oldValue;
}
