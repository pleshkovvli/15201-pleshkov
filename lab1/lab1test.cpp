#include "lab1.hpp"
#include "googletest-master/googletest/include/gtest/gtest.h"
using namespace lab1_pleshkov;
TEST(testTritSet, testTritSetUsual)
{
	uint i,j;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet a(i);
		EXPECT_TRUE(a.getCapacity()==i);
		for(j=0; j<(i+i+1); j++)
		{
			EXPECT_TRUE(a[j]==Unknown);
		}
	}
}

TEST(testTritSet, testTritSetCopy)
{
	uint i,j;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet c(i);
		for(j=0; j<i; j++)
		{
			c[j]=(Trit)((j+1)%3);		//120120120
		}
		TritSet b=c;
		EXPECT_TRUE(b.getCapacity()==c.getCapacity());
		for(j=0; j<i; j++)
		{
			c[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)((j+1)%3));
		}
	}
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet* a=new TritSet(i);
		for(j=0; j<i; j++)
		{
			(*a)[j]=(Trit)(j%3);		//012012012
		}
		TritSet b=*a;
		EXPECT_TRUE(b.getCapacity()==a->getCapacity());
		for(j=0; j<i; j++)
		{
			(*a)[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}
		delete a;
		for(j=0; j<i; j++)
		{
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}				
	}
}

TEST(testTritSet, testTritSetMove)
{
	uint i,j;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet c(i);
		for(j=0; j<i; j++)
		{
			c[j]=(Trit)((j+1)%3);		//120120120
		}
		TritSet b=std::move(c);
		EXPECT_TRUE(0==c.getCapacity());
		EXPECT_TRUE(i==b.getCapacity());
		for(j=0; j<i; j++)
		{
			c[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)((j+1)%3));
		}
	}
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet* a=new TritSet(i);
		for(j=0; j<i; j++)
		{
			(*a)[j]=(Trit)(j%3);		//012012012
		}
		TritSet b=std::move(*a);
		EXPECT_TRUE(0==a->getCapacity());
		EXPECT_TRUE(i==b.getCapacity());
		for(j=0; j<i; j++)
		{
			(*a)[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}
		delete a;
		for(j=0; j<i; j++)
		{
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}				
	}
}

TEST(testTritSet,evaluationUsual)
{
	uint i,j;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet c(i);
		for(j=0; j<i; j++)
		{
			c[j]=(Trit)((j+1)%3);		//120120120
		}
		TritSet b(DEFAULT_SIZE);
		b=c;
		EXPECT_TRUE(b.getCapacity()==c.getCapacity());
		for(j=0; j<i; j++)
		{
			c[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)((j+1)%3));
		}
	}
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet* a=new TritSet(i);
		for(j=0; j<i; j++)
		{
			(*a)[j]=(Trit)(j%3);		//012012012
		}
		TritSet b(DEFAULT_SIZE);
		b=*a;
		EXPECT_TRUE(b.getCapacity()==a->getCapacity());
		for(j=0; j<i; j++)
		{
			(*a)[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}
		delete a;
		for(j=0; j<i; j++)
		{
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}				
	}
}

TEST(testTritSet,evaluationMove)
{
	uint i=0,j=0;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet c(i);
		for(j=0; j<i; j++)
		{
			c[j]=(Trit)((j+1)%3);		//120120120
		}
		TritSet b(DEFAULT_SIZE); 
		b=std::move(c);
		EXPECT_TRUE(0==c.getCapacity());
		EXPECT_TRUE(i==b.getCapacity());
		for(j=0; j<i; j++)
		{
			c[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)((j+1)%3));
		}
	}
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet* a=new TritSet(i);
		for(j=0; j<i; j++)
		{
			(*a)[j]=(Trit)(j%3);		//012012012
		}
		TritSet b(DEFAULT_SIZE); 
		b=std::move(*a);
		EXPECT_TRUE(0==a->getCapacity());
		EXPECT_TRUE(i==b.getCapacity());
		for(j=0; j<i; j++)
		{
			(*a)[j]=Unknown;
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}
		delete a;
		for(j=0; j<i; j++)
		{
			EXPECT_TRUE(b[j]==(Trit)(j%3));
		}				
	}
}

TEST(testTritSet, constBrackets)
{
	const TritSet a(10);
	for(uint i=0; i<(a.getCapacity()*2); i++)
	{
		EXPECT_TRUE(a[i]==Unknown);
	}
}

TEST(testTritSet,testTritSetAnd)
{
	TritSet testSetLittle(DEFAULT_SIZE*UINT_TRIT);
	TritSet testSetBig(2*DEFAULT_SIZE*UINT_TRIT);
	uint i;
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetLittle[i]=Trit(i%3);			//012012012
	}
	for(i=0; i<2*DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetBig[i]=Trit((i+(i%9)/3)%3);	//012120201
	}
	TritSet testSetAnd=(testSetBig&testSetLittle);
	EXPECT_TRUE(testSetAnd.getCapacity()==testSetBig.getCapacity());
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%9)
		{
			case 0: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 1: EXPECT_TRUE(True==testSetAnd[i]); break;
			case 2: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 3: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 4: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 5: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 6: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 7: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 8: EXPECT_TRUE(False==testSetAnd[i]); break;
		}
	}
	for(i=DEFAULT_SIZE*UINT_TRIT; i<2*DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%9)
		{
			case 0: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 1: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 2: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 3: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 4: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 5: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 6: EXPECT_TRUE(False==testSetAnd[i]); break;
			case 7: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
			case 8: EXPECT_TRUE(Unknown==testSetAnd[i]); break;
		}
	}
}

TEST(testTritSet,testTritSetOr)
{
	TritSet testSetLittle(DEFAULT_SIZE*UINT_TRIT);
	TritSet testSetBig(2*DEFAULT_SIZE*UINT_TRIT);
	uint i;
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetLittle[i]=Trit(i%3);			//012012012
	}
	for(i=0; i<2*DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetBig[i]=Trit((i+(i%9)/3)%3);	//012120201
	}
	TritSet testSetOr=testSetBig|testSetLittle;
	EXPECT_TRUE(testSetOr.getCapacity()==testSetBig.getCapacity());
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%9)
		{
			case 0: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 1: EXPECT_TRUE(True==testSetOr[i]); break;
			case 2: EXPECT_TRUE(False==testSetOr[i]); break;
			case 3: EXPECT_TRUE(True==testSetOr[i]); break;
			case 4: EXPECT_TRUE(True==testSetOr[i]); break;
			case 5: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 6: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 7: EXPECT_TRUE(True==testSetOr[i]); break;
			case 8: EXPECT_TRUE(True==testSetOr[i]); break;
		}
	}
	for(i=DEFAULT_SIZE*UINT_TRIT; i<2*DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%9)
		{
			case 0: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 1: EXPECT_TRUE(True==testSetOr[i]); break;
			case 2: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 3: EXPECT_TRUE(True==testSetOr[i]); break;
			case 4: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 5: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 6: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 7: EXPECT_TRUE(Unknown==testSetOr[i]); break;
			case 8: EXPECT_TRUE(True==testSetOr[i]); break;
		}
	}
}

TEST(testTritSet,testTritSetNot)
{
	TritSet testSet(DEFAULT_SIZE*UINT_TRIT);
	uint i;
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSet[i]=Trit(i%3);			//012012012
	}
	TritSet testSetNot=~testSet;
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%3)
		{
			case 0: EXPECT_TRUE(Unknown==testSetNot[i]); break;
			case 1: EXPECT_TRUE(False==testSetNot[i]); break;
			case 2: EXPECT_TRUE(True==testSetNot[i]); break;
		}
	}
}

TEST(testTritSet,testTritSetComplex)
{
	TritSet testSetLittle(DEFAULT_SIZE*UINT_TRIT);
	TritSet testSetBig(3*DEFAULT_SIZE*UINT_TRIT);
	TritSet testSetMiddle(2*DEFAULT_SIZE*UINT_TRIT);
	uint i;
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetLittle[i]=Trit(i%3);				//012012012
	}
	for(i=0; i<2*DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetMiddle[i]=Trit((i+1)%3);			//120120120
	}
	for(i=0; i<3*DEFAULT_SIZE*UINT_TRIT; i++)
	{
		testSetBig[i]=Trit((i+2)%3);			//201201201
	}
	TritSet testSetOne=(testSetLittle&testSetBig)&testSetMiddle;
	EXPECT_TRUE(testSetOne.getCapacity()==testSetBig.getCapacity());
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		EXPECT_TRUE(False==testSetOne[i]);
	}
	TritSet testSetTwo=(testSetLittle|testSetBig)|testSetMiddle;
	EXPECT_TRUE(testSetTwo.getCapacity()==testSetBig.getCapacity());
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		EXPECT_TRUE(True==testSetTwo[i]);
	}
	TritSet testSetThree=(testSetLittle&testSetBig)|testSetMiddle;
	EXPECT_TRUE(testSetThree.getCapacity()==testSetBig.getCapacity());
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%3)
		{
			case 0: EXPECT_TRUE(True==testSetThree[i]); break;
			case 1: EXPECT_TRUE(Unknown==testSetThree[i]); break;
			case 2: EXPECT_TRUE(Unknown==testSetThree[i]); break;
		}
	}
	TritSet testSetFour=(~testSetLittle)&(~testSetBig);
	EXPECT_TRUE(testSetFour.getCapacity()==testSetBig.getCapacity());
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		switch(i%3)
		{
			case 0: EXPECT_TRUE(Unknown==testSetFour[i]); break;
			case 1: EXPECT_TRUE(False==testSetFour[i]); break;
			case 2: EXPECT_TRUE(False==testSetFour[i]); break;
		}
	}	
}

TEST(testTritSet,testShrink)
{
	uint i;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet a(i);
		EXPECT_TRUE(a.getCapacity()==i);
		a[i/3]=True;
		a[2*i/3]=False;
		EXPECT_TRUE(a.getCapacity()==(i+(i==0)));
		a.shrink();
		EXPECT_TRUE(a.getCapacity()==(2*i/3+1));
		a[2*i/3]=Unknown;
		a.shrink();
		if((i/3)!=(2*i/3))
		{
			EXPECT_TRUE(a.getCapacity()==(i/3+1));
		}
	}	
}

TEST(testTritSet,testTrim)
{
	uint i;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet a(i);
		EXPECT_TRUE(a.getCapacity()==i);
		a[2*i/3]=True;
		a.trim(2*i/3);
		EXPECT_TRUE(a.getCapacity()==2*i/3);
		a[i/3]=False;
		a.trim(i/3);
		EXPECT_TRUE(a.getCapacity()==i/3);
		a[2*i/3+1]=True;
		EXPECT_TRUE(a[i/3]==Unknown);
		EXPECT_TRUE(a[2*i/3]==Unknown);
	}	
}

TEST(testTritSet, testLength)
{
	uint i;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet a(i);
		EXPECT_TRUE(a.length()==0);
		a[2*i/3]=True;
		EXPECT_TRUE(a.length()==(2*i/3+1));
		a[2*i/3]=Unknown;
		a[i/3]=False;
		EXPECT_TRUE(a.length()==(i/3+1));
		a[i/3]=Unknown;
		a[i+i]=True;
		EXPECT_TRUE(a.length()==(i+i+1));
	}
}

TEST(testTritSet, testCardinality)
{
	uint i;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT); i++)
	{
		TritSet a(i);
		for(auto x:a)
		{
			x=Trit(x.getIndex()%3);
		}
		a[3*i]=True;
		a[3*i]=Unknown;
		EXPECT_TRUE(a.cardinality(Unknown)==(i/3+((i%3)==2)));
		EXPECT_TRUE(a.cardinality(True)==(i/3+(i%3==2)));
		EXPECT_TRUE(a.cardinality(False)==(i/3));
		EXPECT_TRUE(a.cardinality().begin(0)->second==(i/3+((i%3)==2)));
		EXPECT_TRUE(a.cardinality().begin(1)->second==(i/3+(i%3==2)));
		EXPECT_TRUE(a.cardinality().begin(2)->second==(i/3));
	}
}



TEST(testTritHolder,testMemory)
{
	TritSet testSet(0);
	uint i;
	size_t currentSize=0;
	for(i=0; i<DEFAULT_SIZE*UINT_TRIT; i++)
	{
		currentSize=testSet.getCapacity();
		testSet[i]=Unknown;
		EXPECT_TRUE(testSet[i]==Unknown);
		EXPECT_TRUE(testSet.getCapacity()==currentSize);
		if(i%2==0)
		{
			testSet[i]=True;
			EXPECT_TRUE(testSet[i]==True);
		}
		else
		{
			testSet[i]=False;
			EXPECT_TRUE(testSet[i]==False);
		}
		EXPECT_TRUE(testSet.getCapacity()>currentSize);
		currentSize=testSet.getCapacity();
		testSet[i]=Unknown;
		EXPECT_TRUE(testSet[i]==Unknown);
		EXPECT_TRUE(testSet.getCapacity()==currentSize);
	}
}

TEST(testTritHolder,testAccurateValues)
{
	TritSet testSet(DEFAULT_SIZE*UINT_TRIT);
	Trit a,b,c;
	uint i;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT-2); i++)
	{
		testSet[i]=Unknown;
		testSet[i+1]=True;
		testSet[i+2]=False;
		a=testSet[i];
		b=testSet[i+1];
		c=testSet[i+2];
		EXPECT_TRUE(testSet[i]==Unknown);
		EXPECT_TRUE(testSet[i+1]==True);
		EXPECT_TRUE(testSet[i+2]==False);
		EXPECT_TRUE(a==Unknown);
		EXPECT_TRUE(b==True);
		EXPECT_TRUE(c==False);
		testSet[i]=True;
		testSet[i+1]=False;
		testSet[i+2]=Unknown;
		a=testSet[i];
		b=testSet[i+1];
		c=testSet[i+2];
		EXPECT_TRUE(testSet[i]==True);
		EXPECT_TRUE(testSet[i+1]==False);
		EXPECT_TRUE(testSet[i+2]==Unknown);
		EXPECT_TRUE(a==True);
		EXPECT_TRUE(b==False);
		EXPECT_TRUE(c==Unknown);
		testSet[i]=Unknown;
		testSet[i+1]=True;
		testSet[i+2]=False;
		a=testSet[i];
		b=testSet[i+1];
		c=testSet[i+2];
		EXPECT_TRUE(testSet[i]==Unknown);
		EXPECT_TRUE(testSet[i+1]==True);
		EXPECT_TRUE(testSet[i+2]==False);
		EXPECT_TRUE(a==Unknown);
		EXPECT_TRUE(b==True);
		EXPECT_TRUE(c==False);
	}
}

TEST(testTritHolder,testEvaluation)
{
	TritSet testSet(DEFAULT_SIZE*UINT_TRIT);
	Trit a;
	uint i;
	for(i=0; i<(DEFAULT_SIZE*UINT_TRIT-2); i++)
	{
		testSet[i]=testSet[i+1]=testSet[i+2]=Unknown;
		EXPECT_TRUE(testSet[i]==Unknown);
		EXPECT_TRUE(testSet[i+1]==Unknown);
		EXPECT_TRUE(testSet[i+2]==Unknown);
		a=testSet[i+1]=testSet[i+2]=True;
		EXPECT_TRUE(a==True);
		EXPECT_TRUE(testSet[i+1]==True);
		EXPECT_TRUE(testSet[i+2]==True);
		testSet[i+2]=False;
		a=testSet[i+2];
		EXPECT_TRUE(a==False);
		EXPECT_TRUE(testSet[i+2]==False);
		testSet[i+2]=testSet[i+1]=testSet[i];
		EXPECT_TRUE(testSet[i]==Unknown);
		EXPECT_TRUE(testSet[i+1]==Unknown);
		EXPECT_TRUE(testSet[i+2]==Unknown);
		testSet[i+2]=True;
		a=testSet[i]=testSet[i+1]=testSet[i+2];
		EXPECT_TRUE(a==True);
		EXPECT_TRUE(testSet[i]==True);
		EXPECT_TRUE(testSet[i+1]==True);
		EXPECT_TRUE(testSet[i+2]==True);
	}
}

TEST(testTritSet, testCout)
{
	TritSet a(100);
    for(auto& value : a)
    {
        value = (Trit)(value.getIndex() % 3);
    }
	std::cout << a;
}
