#include "lab1.hpp"
using namespace lab1_pleshkov;

TritSet::TritSet(size_t startCapacity)
{
	if(startCapacity==0)
	{
		zeroSet();
        return;
	}
    tritCapacity=startCapacity;
	arraySize = calculateArraySize(tritCapacity);
	data=new uint[arraySize];
	for(size_t i=0; i<arraySize; i++)
	{
		data[i]=0;
	}
}

TritSet::TritSet(const TritSet& set)
{
	if(set.data==nullptr)
	{
		zeroSet();
		return;
	}
	data=new uint[set.arraySize];
	arraySize=set.arraySize;
	tritCapacity=set.tritCapacity;
	std::copy(set.data, set.data + arraySize, data);
}

TritSet::TritSet(TritSet&& set)
{
	arraySize=set.arraySize;
	tritCapacity=set.tritCapacity;
	data=set.data;
	set.zeroSet();
}

TritSet& TritSet::operator=(const TritSet& set)
{
	if(&set==this)
	{
		return *this;
	}
	if(set.data==nullptr)
	{
		if(data!=nullptr)
		{
			delete [] data;
		}
		arraySize=set.arraySize;
		tritCapacity=set.tritCapacity;
		data=nullptr;
		return *this;
	}
	uint* newData=new uint[set.arraySize];
	arraySize=set.arraySize;
	tritCapacity=set.tritCapacity;
	if(data!=nullptr)
	{
		delete [] data;
	}
	data=newData;
	std::copy(set.data, set.data + arraySize, data);
	return *this;
}


TritSet& TritSet::operator=(TritSet&& set)
{
	if(&set == this)
	{
		return *this;
	}
	arraySize=set.arraySize;
	tritCapacity=set.tritCapacity;
	if(data!=nullptr)
	{
		delete [] data;
	}
	data=set.data;
	set.zeroSet();
	return *this;
}

TritSet::~TritSet()
{
	if(this->data!=nullptr)
	{
		delete [] data;
	}
}

void TritSet::resize(size_t newCapacity)
{
	if(newCapacity==tritCapacity)
	{
		return;
	}
    if(newCapacity==0)
    {
        if(data!=nullptr)
        {
            delete [] data;
        }
        zeroSet();
        return;
    }
    size_t newArraySize=calculateArraySize(newCapacity);
	if(newArraySize==arraySize)
	{
		resizeEqualArrays(newCapacity);
		return;
	}
	resizeUnequalArrays(newCapacity,newArraySize);
}

void TritSet::resizeEqualArrays(size_t newCapacity)
{
    size_t lastTrit=(tritCapacity<newCapacity) ? tritCapacity : newCapacity;
    size_t newLastTrit=(tritCapacity>newCapacity) ? tritCapacity : newCapacity;
    for(size_t j=lastTrit; j<newLastTrit; j++)
    {
        (*this)[j]=Unknown;
    }
    tritCapacity = newCapacity;
}

void TritSet::resizeUnequalArrays(size_t newCapacity, size_t newArraySize)
{
    uint *newData= new uint[newArraySize];
    size_t lastIndex= ((arraySize<newArraySize) ? arraySize : newArraySize);
    if(data!=nullptr)
    {
        std::copy(data, data + lastIndex, newData);
        delete [] data;
    }
    for(size_t i=lastIndex; i<newArraySize; i++)
    {
        newData[i]=0;
    }
    data=newData;
    size_t lastTrit=(tritCapacity<newCapacity) ? tritCapacity : newCapacity;
    size_t newLastTrit=lastIndex*UINT_TRIT;
    for(size_t j=lastTrit; j<newLastTrit; j++)
    {
        (*this)[j]=Unknown;
    }
    arraySize = newArraySize;
    tritCapacity = newCapacity;
}

TritSet::TritHolder TritSet::operator[](size_t index)
{
	return TritHolder(index,*this);
}

TritSet::ConstTritHolder TritSet::operator[](size_t index) const
{
	return ConstTritHolder(index,*this);
}

TritSet lab1_pleshkov::TritSet::operator&(const TritSet& rvalue) const
{
	size_t newCapacity=((tritCapacity>rvalue.tritCapacity) ? tritCapacity : rvalue.tritCapacity);
	TritSet newTritSet(newCapacity);
	for(size_t i=0; i<newCapacity; i++)
	{
		newTritSet[i]=(rvalue[i])&((*this)[i]);
	}
	return std::move(newTritSet);
}

TritSet lab1_pleshkov::TritSet::operator|(const TritSet& rvalue) const
{
	size_t newCapacity=((tritCapacity>rvalue.tritCapacity) ? tritCapacity : rvalue.tritCapacity);
	TritSet newTritSet(newCapacity);
	for(size_t i=0; i<newCapacity; i++)
	{
		newTritSet[i]=(rvalue[i])|((*this)[i]);
	}
	return std::move(newTritSet);
}

TritSet lab1_pleshkov::TritSet::operator~() const
{
	TritSet newTritSet(tritCapacity);
	for(size_t i=0; i<tritCapacity; i++)
	{
		newTritSet[i]=~((*this)[i]);
	}
	return std::move(newTritSet);
}

void TritSet::changeValue(size_t tritIndex, Trit rvalue)
{
	data[tritIndex/UINT_TRIT]=data[tritIndex/UINT_TRIT]&zeroTrit(tritIndex);
	data[tritIndex/UINT_TRIT]|=((uint)rvalue)<<(tritIndex%UINT_TRIT*2);
}

size_t TritSet::calculateArraySize(size_t capacity)
{
    size_t size=capacity/UINT_TRIT+((capacity%UINT_TRIT)>0);
    return size;
}

uint TritSet::zeroTrit(size_t tritIndex)
{
	if((tritIndex%UINT_TRIT)==0)
	{
		return ((uint)(-1)<<TRIT_BIT);
	}
	if((tritIndex%UINT_TRIT)==(UINT_TRIT-1))
	{
		return ((uint)(-1)>>TRIT_BIT);
	}
	return (((uint)(-1)<<((tritIndex%UINT_TRIT+1)*TRIT_BIT))|((uint)(-1)>>((UINT_TRIT-tritIndex%UINT_TRIT)*TRIT_BIT)));
}

Trit TritSet::getValue(size_t index) const
{
	uint number = (data[index/UINT_TRIT]>>(index%UINT_TRIT*TRIT_BIT))&(uint)3;
	switch(number)
	{
		case 1: return True;
		case 2: return False;
		default: return Unknown;
	}
}

size_t TritSet::getCapacity() const
{
	return tritCapacity;
}

void TritSet::shrink()
{
	resize(length());
}

void TritSet::zeroSet()
{
    data=nullptr;
    arraySize=0;
    tritCapacity=0;
}

size_t TritSet::cardinality(Trit value) const
{
	uint i;
	size_t cardinal=0;
	for(i=0; i<length(); i++)
	{
		if((*this)[i]==value)
		{
			cardinal++;
		}
	}
	return cardinal;
}

bool TritSet::operator==(TritSet& other) const
{
    if(tritCapacity!=other.tritCapacity)
    {
        return false;
    }
	for(size_t i=0; i<tritCapacity; i++)
    {
        if((*this)[i]!=other[i])
        {
            return false;
        }
    }
    return true;
}

bool TritSet::operator!=(TritSet& other) const
{
	return !operator==(other);
}

std::unordered_map< Trit, uint, std::hash<Trit> > TritSet::cardinality() const
{
	std::unordered_map< Trit, uint, std::hash<Trit> > result;
	result={{Unknown,0},{True,0},{False,0}};
	uint i;
	for(i=0; i<length(); i++)
	{
		result[(*this)[i]]++;
	}
	return result;
}


void TritSet::trim(size_t lastIndex)
{
	if(lastIndex<tritCapacity)
	{
		resize(lastIndex);
	}
}

size_t TritSet::length() const
{
	size_t lastIndex;
	if(data==nullptr)
	{
		return 0;
	}
	for(lastIndex=(arraySize-1); lastIndex>0; lastIndex--)
	{
		if(data[lastIndex]!=0)
		{
			break;
		}
	}
	for(int i=(UINT_TRIT-1); i>=0; i--)
	{
		if((*this)[lastIndex*UINT_TRIT+i]!=Unknown)
		{
			return lastIndex*UINT_TRIT+i+1;
		}
	}
	return 0;
}

TritSet::ConstIterator TritSet::begin() const
{
	return ConstIterator(this, 0);
}
TritSet::ConstIterator TritSet::end() const
{
	return ConstIterator(this, tritCapacity);
}

TritSet::Iterator TritSet::begin()
{
	return Iterator(this, 0);
}
TritSet::Iterator TritSet::end()
{
	return Iterator(this, tritCapacity);
}

std::ostream& operator << (std::ostream& os, lab1_pleshkov::TritSet& tritSet)
{
    for (const auto& value : tritSet)
    {
        std::size_t show = value;
        os << show;
    }
    return os;
}