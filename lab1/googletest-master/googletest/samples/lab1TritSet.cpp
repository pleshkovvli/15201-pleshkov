#include "lab1.hpp"
using namespace lab1_pleshkov;

TritSet::TritSet(size_t startCapacity)
{
	//std::cout << "c";
	arraySize=startCapacity/uintTrit+((startCapacity%uintTrit)>0);
	try
	{
		data=new uint[arraySize];
	}
	catch(...)
	{
		arraySize=DEFAULT_SIZE;
		data=new uint[DEFAULT_SIZE];
		printf("\nFailed to allocate memory; returned default trit-set\n");
	}
	tritCapacity=arraySize*uintTrit;
	baseSize=arraySize;
}

TritSet::TritSet(TritSet const& set)
{
	//std::cout << "k";
	arraySize=set.arraySize;
	try
	{
		data=new uint[arraySize];
		std::copy(set.data, set.data + arraySize, data);

	}
	catch(...)
	{
		arraySize=DEFAULT_SIZE;
		data=new uint[DEFAULT_SIZE];
		std::copy(set.data, set.data + DEFAULT_SIZE, data);
		printf("\nFailed to allocate memory; returned default trit-set\n");
	}
	tritCapacity=arraySize*uintTrit;
	baseSize=arraySize;
}

TritSet::~TritSet()
{
	//std::cout << "d";
	delete [] data;
}

void TritSet::resize(size_t newCapacity)
{
	if(tritCapacity==newCapacity)
	{
		return;
	}
	size_t newArraySize=newCapacity/uintTrit+((newCapacity%uintTrit)>0);
	try
	{
		uint *newData= new uint[newArraySize];
		size_t lastIndex= ((arraySize<newArraySize) ? arraySize : newArraySize);
		std::copy(data, data + lastIndex, newData);
		arraySize = newArraySize;
		tritCapacity = arraySize*uintTrit;
		delete [] data;
		data=newData;

	}
	catch(...)
	{
		printf("\nFailed to allocate memory; returned this trit-set\n");
		throw 0;
	}
}

TritSet::TritHolder TritSet::operator[](size_t index)
{
	return TritHolder(index,*this);
}

TritSet lab1_pleshkov::TritSet::operator&(TritSet rvalue)
{
	//std::cout << "&" << std::endl;
	size_t newCapacity=((tritCapacity>rvalue.tritCapacity) ? tritCapacity : rvalue.tritCapacity);
	TritSet newTritSet(newCapacity);
	if((newTritSet.tritCapacity)<newCapacity)
	{
		printf("\nFailed to allocate memory; returned default-size trit-set\n");
		newCapacity=newTritSet.tritCapacity;
	}
	uint i;
	for(i=0; i<newCapacity; i++)
	{
		newTritSet[i]=(rvalue[i])&((*this)[i]);
	}
	//std::cout << "&" << std::endl;
	return TritSet(newTritSet);
}

TritSet lab1_pleshkov::TritSet::operator|(TritSet rvalue)
{
	//std::cout << "|" << std::endl;
	size_t newCapacity=((tritCapacity>rvalue.tritCapacity) ? tritCapacity : rvalue.tritCapacity);
	TritSet newTritSet(newCapacity);
	if((newTritSet.tritCapacity)<newCapacity)
	{
		printf("\nFailed to allocate memory; returned default-size trit-set\n");
		newCapacity=newTritSet.tritCapacity;
	}
	uint i;
	for(i=0; i<newCapacity; i++)
	{
		newTritSet[i]=(rvalue[i])|((*this)[i]);
	}
	//std::cout << "|" << std::endl;
	return TritSet(newTritSet);
}

TritSet lab1_pleshkov::TritSet::operator~()
{
	//std::cout << "~" << std::endl;
	size_t newCapacity=tritCapacity;
	TritSet newTritSet(newCapacity);
	if((newTritSet.tritCapacity)<newCapacity)
	{
		printf("\nFailed to allocate memory; returned default-size trit-set\n");
		newCapacity=newTritSet.tritCapacity;
	}
	uint i;
	for(i=0; i<newCapacity; i++)
	{
		newTritSet[i]=~((*this)[i]);
	}
	//std::cout << "~" << std::endl;
	return TritSet(newTritSet);
}

int TritSet::getCapacity()
{
	return tritCapacity;
}

void TritSet::addCapacity(size_t newCapacity)
{
	resize(newCapacity);
}

void TritSet::shrink()
{
	size_t lastIndex;
	for(lastIndex=(arraySize-1); lastIndex>=baseSize; lastIndex--)
	{
		if(data[lastIndex]!=0)
		{
			break;
		}
	}
	try
	{
		resize((lastIndex+1)*uintTrit);
	}
	catch(...)
	{
		printf("\nFailed to resize\n");
	}

}

size_t TritSet::cardinality(Trit value)
{
	uint i;
	size_t cardinal=0;
	for(i=0; i<tritCapacity; i++)
	{
		if((*this)[i]==value)
		{
			cardinal++;
		}
	}
	return cardinal;
}

void TritSet::trim(size_t lastIndex)
{
	try
	{
		resize(lastIndex*uintTrit);
	}
	catch(...)
	{
		printf("\nFailed to resize\n");
	}
}

size_t TritSet::length()
{
	size_t lastIndex;
	for(lastIndex=(arraySize-1); lastIndex>0; lastIndex--)
	{
		if(data[lastIndex]!=0)
		{
			break;
		}
	}
	uint i;
	for(i=(uintTrit-1); i>=0; i--)
	{
		if((*this)[i]!=Unknown)
		{
			return lastIndex*uintTrit+i+1;
		}
	}
	return 0;
}

void TritSet::printSet()
{
	for(uint i=0; i<tritCapacity; i++)
	{
		if((i%uintTrit)==0)
		{
			std::cout << std::endl;
		}
		std::cout << (*this)[i] << " ";
	}
	std::cout << std::endl;
}

TritSet TritSet::tes()
{
	TritSet e(10);
	std::cout << "!";
	return TritSet(e);
}
