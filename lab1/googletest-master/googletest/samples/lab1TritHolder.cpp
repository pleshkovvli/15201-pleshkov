#include "lab1.hpp"
using namespace lab1_pleshkov;

TritSet::TritHolder::TritHolder(size_t index, TritSet& father)
:fatherSet(father)
{
	tritIndex=index;
	if(tritIndex>=fatherSet.tritCapacity)
	{
		tritValue=Unknown;
	}
	else
	{
		switch((fatherSet.data[tritIndex/uintTrit]>>(tritIndex%uintTrit*2))&((uint)3))
		{
		case 1: tritValue=True; break;
		case 2: tritValue=False; break;
		default: tritValue=Unknown;
		}
	}
}

TritSet::TritHolder::~TritHolder()
{

}

uint TritSet::TritHolder::zeroTrit()
{
	if((tritIndex%uintTrit)==0)
	{
		return ((uint)(-1)<<2);
	}
	if((tritIndex%uintTrit)==(uintTrit-1))
	{
		return ((uint)(-1)>>2);
	}
	return (((uint)(-1)<<((tritIndex%uintTrit+1)*2))|((uint)(-1)>>((uintTrit-tritIndex%uintTrit)*2)));
}

Trit TritSet::TritHolder::operator=(const Trit& rvalue)
{
	if(tritIndex>=fatherSet.tritCapacity)
	{
		if(rvalue==Unknown)
		{
			return Unknown;
		}
		try
		{
			fatherSet.addCapacity(tritIndex+1);
		}
		catch(...)
		{
			printf("Failed to write trit\n");
			return rvalue;
		}
	}
	if(rvalue==tritValue)
	{
		return rvalue;
	}
	fatherSet.data[tritIndex/uintTrit]=fatherSet.data[tritIndex/uintTrit]&zeroTrit();
	fatherSet.data[tritIndex/uintTrit]|=((uint)rvalue)<<(tritIndex%uintTrit*2);
	return rvalue;
}

Trit TritSet::TritHolder::operator=(const TritHolder& rvalue)
{
	if(tritIndex>=fatherSet.tritCapacity)
	{
		if(rvalue==Unknown)
		{
			return Unknown;
		}
		try
		{
			fatherSet.addCapacity(tritIndex+1);
		}
		catch(...)
		{
			printf("Failed to write trit\n");
			return rvalue;
		}
	}
	if(rvalue==tritValue)
	{
		return rvalue;
	}
	fatherSet.data[tritIndex/uintTrit]=fatherSet.data[tritIndex/uintTrit]&zeroTrit();
	fatherSet.data[tritIndex/uintTrit]|=((uint)rvalue)<<(tritIndex%uintTrit*2);
	return rvalue;
}

TritSet::TritHolder::operator Trit() const
{
	return tritValue;
}




