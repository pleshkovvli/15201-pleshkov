#include "lab1.hpp"
using namespace lab1_pleshkov;

Trit lab1_pleshkov::operator&(Trit one, Trit other)
{
	if((one==False)||(other==False))
	{
		return False;
	}
	if((one==True)&&(other==True))
	{
		return True;
	}
	return Unknown;
}
Trit lab1_pleshkov::operator|(Trit one, Trit other)
{
	if((one==True)||(other==True))
	{
		return True;
	}
	if((one==False)&&(other==False))
	{
		return False;
	}
	return Unknown;
}

Trit lab1_pleshkov::operator~(Trit value)
{
	if(value==True)
	{
		return False;
	}
	if(value==False)
	{
		return True;
	}
	return Unknown;
}
