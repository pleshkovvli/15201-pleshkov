#pragma once

#include <stdio.h>
#include <iostream>
#include <limits.h>
#include <algorithm>
#include <utility>
#include <unordered_map>
#include <functional>
#include <stdexcept>

namespace lab1_pleshkov
{
    enum Trit {False=2, Unknown=0, True=1};
}

namespace std
{
    using Trit = lab1_pleshkov::Trit;
    template <>
    struct hash<Trit>
    {
        std::size_t operator()(const Trit& k) const
        {
            size_t value = k;
            return value;
        }
    };
}

namespace lab1_pleshkov
{
    using uint = unsigned int;
    const uint TRIT_BIT = 2;
    const uint UINT_TRIT=CHAR_BIT*sizeof(uint)/TRIT_BIT;
    const uint DEFAULT_SIZE = 64;
class TritSet
{
public:
    class ConstTritHolder
    {
    public:
        ConstTritHolder(size_t index, const TritSet& father);
        ~ConstTritHolder();
        operator Trit() const;
        bool operator==(const ConstTritHolder& other) const;
        bool operator!=(const ConstTritHolder& other) const;
        const TritSet& getSet() const;
        size_t getIndex() const;
        void setPosition(size_t position);
    private:
        Trit tritValue;
        size_t tritIndex;
        const TritSet& fatherSet;
    };
	class TritHolder
	{
	public:
		TritHolder(size_t index, TritSet& father);
		~TritHolder();
		TritHolder& operator=(const Trit& rvalue);
		TritHolder& operator=(const TritHolder& rvalue);
        TritHolder& operator=(const TritSet::ConstTritHolder& rvalue);
        bool operator==(const TritHolder& other) const;
        bool operator!=(const TritHolder& other) const;
		operator Trit() const;
		TritSet& getSet() const;
        size_t getIndex() const;
		void setPosition(size_t position);
    private:
        Trit tritValue;
        size_t tritIndex;
        TritSet& fatherSet;
	};
    class ConstIterator
    {
    public:
        ConstIterator(const TritSet* set, size_t pos);
        ConstIterator(const ConstIterator& iterator);
        ~ConstIterator();
        bool operator!=(const ConstIterator& other) const;
        bool operator==(const ConstIterator& other) const;
        TritSet::ConstIterator& operator=(const ConstIterator& rvalue);
        TritSet::ConstTritHolder& operator*() const;
        TritSet::ConstTritHolder* operator->() const;
        const ConstIterator& operator++();
        const ConstIterator operator++(int);
    private:
        size_t position;
        const TritSet* mainSet;
        ConstTritHolder* holder;
    };
    class Iterator
    {
    public:
        Iterator(TritSet* set, size_t pos);
        Iterator(const Iterator& iterator);
        ~Iterator();
        bool operator!=(const Iterator& other) const;
        bool operator==(const TritSet::Iterator& other) const;
        TritSet::Iterator& operator=(const Iterator& rvalue);
        TritSet::TritHolder& operator*();
        TritSet::TritHolder* operator->();
        const Iterator& operator++();
        const Iterator operator++(int);
    private:
        TritHolder* holder;
        size_t position;
        TritSet* mainSet;
    };
private:
    void resize(size_t newCapacity);
    void resizeEqualArrays(size_t newCapacity);
    void resizeUnequalArrays(size_t newCapacity, size_t newArraySize);
    void zeroSet();
    size_t calculateArraySize(size_t capacity);
public:
	TritSet(size_t startCapacity);
	TritSet(const TritSet& set);
	TritSet(TritSet&& set);
	TritSet& operator=(const TritSet& set);
	TritSet& operator=(TritSet&& set);
	~TritSet();
	TritHolder operator[](size_t index);
	ConstTritHolder operator[](size_t index) const;
	TritSet operator&(const TritSet& rvalue) const;
	TritSet operator|(const TritSet& rvalue) const;
	TritSet operator~() const;
    bool operator==(TritSet& other) const;
	bool operator!=(TritSet& other) const;
    void changeValue(size_t index, Trit value);
    uint zeroTrit(size_t tritIndex);
    Trit getValue(size_t index) const;
	size_t getCapacity() const;
	void shrink();
	size_t cardinality(Trit value) const;
	std::unordered_map< Trit, uint, std::hash<Trit> > cardinality() const;
	void trim(size_t lastIndex);
	size_t length() const;
	ConstIterator begin() const;
	ConstIterator end() const;
	Iterator begin();
	Iterator end();
private:
    size_t tritCapacity;
    size_t arraySize;
    uint *data;
};

Trit operator&(Trit one, Trit other);
Trit operator|(Trit one, Trit other);
Trit operator~(Trit value);

}

std::ostream& operator << (std::ostream& os, lab1_pleshkov::TritSet& tritSet);
