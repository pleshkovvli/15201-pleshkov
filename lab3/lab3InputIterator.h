#pragma once

#include "lab3.h"
#include "lab3SetTuple.h"

namespace lab3_pleshkov {

    template <typename ... Args>
    CSV_Parser<Args...>::InputIterator::InputIterator(CSV_Parser<Args...>& par,std::ifstream &file, bool isV, bool isE)
            :inputFile(file), parcer(par) {
        isValid = isV;
        isEnd = isE;
        if(isValid) {
            operator++();
        }
    }

    template <typename ... Args>
    CSV_Parser<Args...>::InputIterator::~InputIterator() {}

    template <typename ... Args>
    bool CSV_Parser<Args...>::InputIterator::operator==(const InputIterator &other) const {
        if(!isValid && !other.isValid) {
            return true;
        }
        return false;
    }

    template <typename ... Args>
    bool CSV_Parser<Args...>::InputIterator::operator!=(const InputIterator &other) const {
        return !operator==(other);
    }

    template <typename ... Args>
    std::tuple<Args...>& CSV_Parser<Args...>::InputIterator::operator*() {
        return tupleValue;
    }

    template <typename ... Args>
    typename
    CSV_Parser<Args...>::InputIterator& CSV_Parser<Args...>::InputIterator::operator++() {
        if(isEnd||inputFile.eof()) {
            isValid = false;
            return *this;
        }
        std::string string("");
        std::stringstream stringstream;

        char buf = 0;
        char end = parcer.lineDivider;
        inputFile.get(buf);
        if(buf==parcer.escape) {
            end = parcer.escape;
            string.push_back(buf);
            inputFile.get(buf);
            if(buf==parcer.escape) {
                end = parcer.lineDivider;
                string.push_back(buf);
                inputFile.get(buf);
            }
        }
        while((buf!=end || buf==parcer.escape) && (!inputFile.eof())) {
            string.push_back(buf);
            inputFile.get(buf);
            if(buf == parcer.escape) {
                string.push_back(buf);
                inputFile.get(buf);
                if(buf == parcer.escape) {
                    string.push_back(buf);
                    inputFile.get(buf);
                } else if(end == parcer.escape) {
                    if(buf == parcer.divider) {
                        end = parcer.lineDivider;
                        continue;
                    } else if(buf == parcer.lineDivider) {
                        break;
                    }
                } else {
                    end = parcer.escape;
                }
            }
        }

        if(inputFile.eof()) {
            if(string.size()>0) {
                string.pop_back();
            } else {
                isValid = false;
                return *this;
            }
            stringstream << string;
            parcer.position++;
            setTuple<0, sizeof...(Args) - 1, Args... >::doSetTuple(stringstream,tupleValue,parcer.divider,parcer.escape);

            isEnd = true;
            return *this;
        }
        stringstream << string;
        parcer.position++;
        setTuple<0, sizeof...(Args) - 1, Args... >::doSetTuple(stringstream,tupleValue,parcer.divider,parcer.escape);
        return *this;
    }
}
