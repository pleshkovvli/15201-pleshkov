#pragma once

#include "lab3.h"

using namespace lab3_pleshkov;

template <typename ... Args>
CSV_Parser<Args...>::CSV_Parser(std::ifstream &file, char div, char esc, char lineDiv)
        :inputFile(file) {
    position = 0;
    if(div==esc || div==lineDiv || esc == lineDiv) {
        throw BadInput("All dividers should be different");
    }
    divider = div;
    escape = esc;
    lineDivider = lineDiv;
}

template <typename ... Args>
CSV_Parser<Args...>::~CSV_Parser() {}

template <typename ... Args>
size_t CSV_Parser<Args...>::getPosition() {
    return position;
}

template <typename ... Args>
typename
CSV_Parser<Args...>::InputIterator CSV_Parser<Args...>::begin() {
    return CSV_Parser<Args...>::InputIterator(*this,inputFile, true, false);
}

template <typename ... Args>
typename
CSV_Parser<Args...>::InputIterator CSV_Parser<Args...>::end() {
    return CSV_Parser<Args...>::InputIterator(*this,inputFile, false, true);
}
