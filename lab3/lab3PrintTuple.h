#pragma once

#include "lab3.h"

namespace lab3_pleshkov {
    template <size_t N, size_t L, typename ... Args>
    struct printTuple {
        static void doPrint(std::ostream& os, const std::tuple<Args...>& t) {
            os << std::get<N>(t) << " ";
            printTuple<N+1,L,Args...>::doPrint(os,t);
        }
    };

    template <size_t N, typename ... Args>
    struct printTuple<N,N,Args...> {
        static void doPrint(std::ostream& os, const std::tuple<Args...>& t) {
            os << std::get<N>(t);
        }
    };

    template <typename ... Args>
    std::ostream& operator<<(std::ostream& os, const std::tuple<Args...>& t) {
        typedef std::tuple<Args...> tupleType ;
        printTuple<0, std::tuple_size<tupleType >::value - 1, Args... >::doPrint(os,t);
        return os;
    };
}