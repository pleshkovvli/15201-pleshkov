#pragma once

#include <fstream>
#include <iostream>
#include <tuple>
#include <sstream>

namespace lab3_pleshkov {
    class BadData : public std::exception {
    public:
        size_t column;
    };

    class BadInput : public std::exception {
    public:
        BadInput() {
            message = "Unknown";
        }
        BadInput(std::string mes) {
            message = mes;
        }
        std::string& getMessage() {
            return message;
        }
    private:
        std::string message;
    };


    template <typename T>
    T getFirst(std::stringstream& stringstream, char divider, char escape) {
        std::stringstream sstream(getFirst<std::string>(stringstream,divider,escape));
        T value;
        sstream >> value;
        if(!sstream.eof()) {
            throw BadData();
        }
        if(!stringstream.badbit) {
            throw BadData();
        }
        return value;
    }

    template <>
    std::string getFirst(std::stringstream& stringstream, char divider, char escape) {
        std::string value("");
        int buf = 0;
        buf = stringstream.get();
        char end = divider;
        if(buf==escape) {
            end = escape;
            buf = stringstream.get();
            while(buf == escape) {
                buf = stringstream.get();
                if(buf==divider) {
                    return value;
                } else if(buf == escape) {
                    value.push_back(buf);
                    buf = stringstream.get();
                } else {
                    throw BadData();
                }
            }
        }
        bool flag = false;
        while((buf!=end) && (!stringstream.eof())) {
            value.push_back(buf);
            buf = stringstream.get();
            while(buf == escape && end==escape) {
                buf = stringstream.get();
                if(buf == escape) {
                    if(!stringstream.eof()) {
                        value.push_back(buf);
                        buf = stringstream.get();
                    }
                } else if(buf == divider) {
                    flag = true;
                    break;
                } else if(!stringstream.eof()) {
                    throw BadData();
                }
            }
            if(flag) {
                break;
            }
        }
        return value;
    }

    template <>
    char getFirst(std::stringstream& stringstream, char divider, char escape) {
        std::stringstream sstream(getFirst<std::string>(stringstream,divider,escape));
        char value;
        sstream >> value;
        return value;
    }

    template <typename ... Args>
    class CSV_Parser {
    public:
        CSV_Parser(std::ifstream& file, char div, char esc, char lineDivider);
        ~CSV_Parser();
        class InputIterator {
        public:
            InputIterator(CSV_Parser& par, std::ifstream& file, bool isV, bool isE);
            ~InputIterator();
            bool operator==(const InputIterator& other) const;
            bool operator!=(const InputIterator& other) const;
            InputIterator& operator++();
            std::tuple<Args...>& operator*();
        private:
            bool isValid;
            bool isEnd;
            std::ifstream& inputFile;
            std::tuple<Args...> tupleValue;
            CSV_Parser<Args...>& parcer;
        };
        size_t getPosition();
        InputIterator begin();
        InputIterator end();
    private:
        std::ifstream& inputFile;
        size_t position;
        char divider;
        char escape;
        char lineDivider;
    };
}
