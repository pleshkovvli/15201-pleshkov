#pragma once

#include "lab3.h"

namespace lab3_pleshkov {

    template <size_t N, size_t L, typename ... Args>
    struct setTuple {
        static void doSetTuple(std::stringstream& os, std::tuple<Args...>& t, char divider, char escape) {
            try {
                std::get<N>(t) = getFirst<typename std::tuple_element<N, std::tuple<Args...> >::type>(os, divider, escape);
            } catch (BadData& e) {
                e.column = N + 1;
                throw;
            } catch(...) {
                throw;
            }
            setTuple<N+1,L, Args...>::doSetTuple(os, t, divider,escape);
        }
    };
    template <size_t N, typename ... Args>
    struct setTuple<N, N, Args...> {
        static void doSetTuple(std::stringstream& os, std::tuple<Args...>& t, char divider, char escape) {
            try {
                std::get<N>(t) = getFirst<typename std::tuple_element<N, std::tuple<Args...> >::type>(os, divider, escape);
            } catch (BadData& e) {
                e.column = N;
                throw;
            }
        }
    };
}