#include "lab1.hpp"
using namespace lab1_pleshkov;

TritSet::TritHolder::TritHolder(size_t index, TritSet& father)
:fatherSet(father)
{
	tritIndex=index;
	if(tritIndex>=fatherSet.tritCapacity)
	{
		tritValue=Unknown;
	}
	else
	{
		switch((fatherSet.data[tritIndex/UINT_TRIT]>>(tritIndex%UINT_TRIT*2))&((uint)3))
		{
		case 1: tritValue=True; break;
		case 2: tritValue=False; break;
		default: tritValue=Unknown;
		}
	}
}

TritSet::TritHolder::~TritHolder()
{

}

uint TritSet::TritHolder::zeroTrit()
{
	if((tritIndex%UINT_TRIT)==0)
	{
		return ((uint)(-1)<<2);
	}
	if((tritIndex%UINT_TRIT)==(UINT_TRIT-1))
	{
		return ((uint)(-1)>>2);
	}
	return (((uint)(-1)<<((tritIndex%UINT_TRIT+1)*2))|((uint)(-1)>>((UINT_TRIT-tritIndex%UINT_TRIT)*2)));
}

Trit TritSet::TritHolder::operator=(const Trit& rvalue)
{
	if(tritIndex>=fatherSet.tritCapacity)
	{
		if(rvalue==Unknown)
		{
			return Unknown;
		}
		try
		{
			fatherSet.addCapacity(tritIndex+1);
		}
		catch(...)
		{
			printf("Failed to write trit\n");
			return rvalue;
		}
	}
	if(rvalue==tritValue)
	{
		return rvalue;
	}
	fatherSet.data[tritIndex/UINT_TRIT]=fatherSet.data[tritIndex/UINT_TRIT]&zeroTrit();
	fatherSet.data[tritIndex/UINT_TRIT]|=((uint)rvalue)<<(tritIndex%UINT_TRIT*2);
	return rvalue;
}

Trit TritSet::TritHolder::operator=(const TritHolder& rvalue)
{
	if(tritIndex>=fatherSet.tritCapacity)
	{
		if(rvalue==Unknown)
		{
			return Unknown;
		}
		try
		{
			fatherSet.addCapacity(tritIndex+1);
		}
		catch(...)
		{
			printf("Failed to write trit\n");
			return rvalue;
		}
	}
	if(rvalue==tritValue)
	{
		return rvalue;
	}
	fatherSet.data[tritIndex/UINT_TRIT]=fatherSet.data[tritIndex/UINT_TRIT]&zeroTrit();
	fatherSet.data[tritIndex/UINT_TRIT]|=((uint)rvalue)<<(tritIndex%UINT_TRIT*2);
	return rvalue;
}

TritSet::TritHolder::operator Trit() const
{
	return tritValue;
}




