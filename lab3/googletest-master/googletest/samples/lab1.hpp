#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <limits.h>
#include <algorithm>

#ifndef LAB1_HPP_
#define LAB1_HPP_

#define DEFAULT_SIZE 64

namespace lab1_pleshkov
{
typedef unsigned int uint;
enum Trit {False=2, Unknown=0, True=1};

//class TritHolder;

const uint UINT_TRIT=CHAR_BIT*sizeof(uint)/2;

class TritSet
{
private:
	class TritHolder
	{
	private:
		Trit tritValue;
		size_t tritIndex;
		TritSet& fatherSet;
		uint zeroTrit();
	public:
		TritHolder(size_t index, TritSet& father);
		~TritHolder();
		Trit operator=(const Trit& rvalue);
		Trit operator=(const TritHolder& rvalue);
		operator Trit() const;
	};
	size_t baseSize;
	size_t tritCapacity;
	size_t arraySize;
	uint *data;
	void resize(size_t newCapacity);
	void addCapacity(size_t newCapacity);
public:
	TritSet(size_t startCapacity);
	TritSet(TritSet const& set);
	~TritSet();
	TritHolder operator[](size_t index);
	TritSet operator&(TritSet rvalue);
	TritSet operator|(TritSet rvalue);
	TritSet operator~();
	int getCapacity();
	void shrink();
	size_t cardinality(Trit value);
	void trim(size_t lastIndex);
	size_t length();
	void printSet();
	TritSet tes();
protected:
};

const Trit operator&(const Trit& lvalue, const Trit& rvalue);
const Trit operator|(const Trit& lvalue, const Trit& rvalue);
const Trit operator~(const Trit& value);
}

#endif
