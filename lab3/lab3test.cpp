#include "lab3final.h"
#include "googletest-master/googletest/include/gtest/gtest.h"

using namespace lab3_pleshkov;

void checkFiles(const std::string& testFile) {
    std::fstream testOut, actualOut;
    testOut.open(testFile);
    actualOut.open("out.txt");
    char testValue, actualValue;
    while(actualOut.get(testValue)) {
        testOut.get(actualValue);
        EXPECT_EQ(testValue,actualValue);
    }
}

TEST(test1, testOne)
{
    std::ifstream fileIn("test1in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<int, char, std::string> par(fileIn,',','\\', '\n');
    try {
        for(const auto& rs : par) {
           fileOut << rs << std::endl;
        }
    }
    catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test1out");
}

TEST(test2, testTwo)
{
    std::ifstream fileIn("test2in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<int> par(fileIn,'.','\\', ' ');
    try {
        for(const auto& rs : par) {
            fileOut << rs << std::endl;
        }
    }
    catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test2out");
}

TEST(test3, testThird)
{
    std::ifstream fileIn("test3in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<char, double, double, std::string> par(fileIn,'.','\\', '!');
    try {
        for(const auto& rs : par) {
            fileOut << rs << std::endl;
        }
    }
    catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test3out");
}


TEST(test4, testFourth)
{
    std::ifstream fileIn("test4in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<char, std::string> par(fileIn,'l','\\', 'u');
    try {
        for(const auto& rs : par) {
            fileOut << rs << std::endl;
        }
    }
    catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test4out");
}

TEST(test5, testFifth)
{
    std::ifstream fileIn("test5in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<int, double> par(fileIn,',','\\', '\n');
    try {
        for(const auto& rs : par) {
            fileOut << rs << std::endl;
        }
    }
    catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test5out");
}

TEST(test6, testSixth)
{
    std::ifstream fileIn("test6in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<int, char, std::string > par(fileIn,',','\"', '\n');
    try {
        for(const auto& rs : par) {
        fileOut << rs << std::endl;
        }
    } catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test6out");
}

TEST(test7, testSeventh)
{
    std::ifstream fileIn("test7in");
    std::ofstream fileOut("out.txt");
    CSV_Parser<char, std::string > par(fileIn,',','\"', '\n');
    try {
        for(const auto& rs : par) {
            fileOut << rs << std::endl;
        }
    } catch (const BadData& e) {
        fileOut << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        fileOut << "Failed: unknown" << std::endl;
    }
    checkFiles("test7out");
}

TEST(test8, testEighth) {
    std::ifstream fileIn("test8in");
    std::ofstream fileOut("out.txt");
    try {
        CSV_Parser<int, double > par(fileIn,',',',', '\n');
    } catch(BadInput& e) {
        fileOut << e.getMessage();
    }
    checkFiles("test8out");
}