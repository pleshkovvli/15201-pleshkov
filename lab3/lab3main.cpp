#include <sstream>
#include "lab3.h"
#include "lab3Parcer.h"

using namespace lab3_pleshkov;

int main() {
    std::ifstream file("test.txt");
    CSV_Parcer<int, char, std::string> par(file,',','\\','!');
    size_t i = 0;
    try {
        for(auto rs : par) {
            std::cout << rs;
        }
    }
    catch (BadData& e) {
        std::cout << "Failed: bad input " << par.getPosition() << " " << e.column << std::endl;
    } catch (...) {
        std::cout << "Failed: unknown" << std::endl;
    }
    i++;
}